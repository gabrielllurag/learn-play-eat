import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';

import SignInScreen from './src/screens/auth/SignIn'
import SignUpScreen from './src/screens/auth/SignUp'
import CreateAccountScreen from './src/screens/auth/CreateAccount'
import OnBoard from './src/screens/Onboading/OnBoard';
import App from './src/App'

const Stack = createNativeStackNavigator();
const options = {
    headerStyle: {backgroundColor: '#FFFF00',},
    headerTintColor: 'black',
    headerTitleAlign: 'center',
    headerBackTitle: ""
}

export default function Main() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen options={{ headerShown: false }} name="OnBoarding" component={OnBoard} />
        <Stack.Screen options={options} name="SignUp" component={SignUpScreen} />
        <Stack.Screen options={options} name="SignIn" component={SignInScreen} />
        <Stack.Screen options={options} name="CreateAccount" component={CreateAccountScreen} />
        <Stack.Screen options={{ headerShown: false, gestureEnabled: false }} name="TabScreen" component={App} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}
