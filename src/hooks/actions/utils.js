import { API_URL, CLOUDINARY_KEY, CLOUDINARY_SECRET, CLOUDINARY_UPLOAD_URL } from '../config'
import moment from 'moment'
import sha1 from 'crypto-js/sha1'

const utilsActions = {
  request: async(store, method = 'GET', url = '', data = {}, headers = {}) => {
    try {
      const options = {
        method,
        body: method != 'GET' ? JSON.stringify(data) : null,
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': store.state.token ? `Bearer ${store.state.token}` : null,
          ...headers
        }
      }
      const res = await fetch(url, options)
      const result = await res.json()
      return result
    } catch (error) {
      return false
    }
  },
  requestAPI: async(store, method = 'GET', url = '/', data = {}, headers = {}) => {
    return await store.actions.request(method, API_URL + url, data, headers)
  },
  cloudinaryUpload: async(uri = '', folder = 'experiments') => {
    var timestamp = moment().format('X').toString();
    var keys = 'folder=' + folder + '&public_id=' + timestamp + '&timestamp=' + timestamp + CLOUDINARY_SECRET;
    var signature = sha1(keys).toString()
    const formdata = new FormData();

    // identify extension
    let ext = uri.split('.').pop();
    let extensions = {
      'jpg': 'image/jpg',
      'png': 'image/png'
    }

    // set form data
    formdata.append('file', {uri: uri, type: extensions[ext], name: `upload.${ext}`});
    formdata.append('public_id', timestamp);
    formdata.append('folder', folder);
    formdata.append('timestamp', timestamp);
    formdata.append('api_key', CLOUDINARY_KEY);
    formdata.append('signature', signature);

    try {
      let response = await fetch(CLOUDINARY_UPLOAD_URL + 'image/upload', {
        method: 'POST',
        body: formdata
      })
      return await response.json()
    } catch (e) {
      console.log('REQUEST_FAILURE', e)
      return { status: false, error: e }
    }
  }
}

export default utilsActions
