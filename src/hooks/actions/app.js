import AsyncStorage from '@react-native-async-storage/async-storage'
import actions from '.'

const appActions = {
  getExperiments: async(store) => {
    const reply = await store.actions.requestAPI('POST','/experiments', {})
    if (reply.success) {
      store.setState({ experiments: reply.data  })
    }
    return reply
  },
  setExperiment: async(store, experiment) => {
    store.setState({
      experiment,
      activities: experiment.activities,
      categories: Object.keys(experiment.activities),
      score: 0,
      totalScore: 0
    })
  },
  saveTried: async(store, food, tries) => {

    const params = {
      food_id: food.id,
      activities: tries
    }
    
    if(food) {
      const res = await store.actions.requestAPI('POST', '/add_tried', { params }, {})

      if(res.success) {
        store.setState({ progress: res.data })
      } else {
        console.log(res)
      }
    }
    
  },
  setSubscribed: async(store) => {
    store.setState({ isSubscribed: true })
  },
  setSubs: async(store, expiration, useCredits) => {
    const reply = await store.actions.requestAPI('POST', '/update_subs', { expiration, useCredits })
    if (reply.success) {
      store.setState({ user: reply.data, level: reply.data.level })
      alert(`We have used your (${useCredits}) available credits to expand yout access for (${usedCredits}) month(s). Continue to invite your friends to get additional FREE month(s) access.`)
    } else {
      alert(reply.message)
    }
  },
  saveSubs: async(store, data) => {
    const res = await store.actions.requestAPI('POST', '/subs_data', { data })
  },
  setGroupID: async(store, groupId) => {
    store.setState({ groupId: groupId })
  },
  saveProgress: async(store) => {
    const { score, experiment, groupId, totalScore, photos, user } = store.state

    const params = {
      user_id: user.id,
      score,
      photos,
      group_id: groupId ? groupId : '',
      experiment_id: experiment.id
    }

    const reply = await store.actions.requestAPI('POST', '/add_progress', params)

    console.log("SAVE PROGRESS PARAMS", params, reply)

    if (reply.success) {
      store.setState({ totalScore: score + totalScore, level: reply.data.level })
    } else {
      alert(reply.message)
    }
  },
  getProgress: async(store) => {
    try {
      const item = await AsyncStorage.getItem('progress')
      if(item != null) {
        const progresses = JSON.parse(item)
        store.setState({ progress: progresses })
      } else {
        store.setState({ progress: [] })
      }
    } catch(e) {
      console.log(e)
    }
  },
  addToProgress: async(store, item) => {
    store.setState({ progress: item })
  },
  reset: async(store) => {
    store.setState({ 
      experiment: false, 
      currentFood: false, 
      activity: false, 
      photos: [],
      userTrieds: [],
      trieds: []
    })
  },
  addToTrieds: async(store, item) => {
    store.setState({ trieds: [...store.state.trieds, item] })
    console.log("tried", store.state.trieds)
  },
  setScore: async(store, score) => {
    store.setState({ score: store.state.score + score })
  },
  resetTrieds: async(store, tried) => {
    store.setState({ userTrieds: [] })
  },
  setCurrentFood: async(store, current) => {
    store.setState({ currentFood: current })
  },
  saveSubs: async(store, data) => {
    const reply = await store.actions.requestAPI('POST', '/subs_data', { data })
  },
  getActivities: async(store, activities) => {
    store.setState({ activities: activities })
  },
  setActivity: async(store, activity) => {
    store.setState({ activity: activity })
  },
  setFoods: async(store, foods) => {
    store.setState({ foods: foods })
  },
  setSelectedFoods: async(store, foodOrders) => {
    store.setState({ foodOrders: foodOrders })
  },
  setCurrentFoods: async(store, currentFoods) => {
    store.setState({ selectedFoods: currentFoods })
  },
  getHelpItems: async(store) => {
    const reply = await store.actions.requestAPI('GET', '/help', {}, {})
    if (reply.success) {
      store.setState({ helpItems: reply.data })
    }
    return reply
  },
  getPhotos: async(store, photo) => {
    store.setState({ photos: [...store.state.photos, photo] })
  },
  handleSubscribe: async(store, subscribe) => {
    store.setState({ isSubscribed: subscribe })
  },
  getUser: async(store) => {
    let reply = await store.actions.requestAPI('POST', '/user')
    if(reply.success){
      reply.data.id = `${reply.data.id}`
      store.setState({ user: reply.data, level: reply.level })
    } else {
      console.log(reply.message)
    }
  },
  sendInvitation: async(store, email, mobile) => {
    const reply = await store.actions.requestAPI('/invite', { email, mobile })
    console.log("INVITE", reply)
    if(reply.success) {
      actions.getUser()
    } else {
      alert(reply.message)
    }
  },
  claimCredits: async(store, email) => {
    const reply = await store.actions.requestAPI('/credits', { email })
    if(reply.success) {
      actions.getUser()
    } else {
      alert(res.message)
    }
  },
  logout: async(store) => {
    try {
      await AsyncStorage.clear()
    } catch(e) {
      console.log(e)
    }
    store.setState({ 
      user: false, 
      progress: [], 
      experiment: false, 
      foods: false,
      foodOrders: false,
      experiments: [],
      userTrieds: [],
      photos: [],
      activities: [],
      activity: false
    })
  },
  login: async(store, email, password) => {
    const reply = await store.actions.requestAPI('POST', '/login', { email, password })
    const storeUser = async (value) => {
      try {
        const jsonValue = JSON.stringify(value)
        await AsyncStorage.setItem('user', jsonValue)
      } catch (e) {
        console.log(e)
      }
    }

    if (reply.success && reply.data.token) {
      store.setState({
        token: reply.data.token,
        user: reply.data.user,
        isLoggedIn: true,
      })
      storeUser({ ...reply.data.user, password })
    }
    return reply
  },
  appleLogin: async(store, user) => {
    const reply = await store.actions.requestAPI('POST', '/apple', { user })
    if(reply.success && reply.data.token) {
      store.setState({
        token: reply.data.token,
        user: reply.data.user,
        isLoggedIn: true,
        apple: true
      })
    }
    const storeUser = async (value) => {
      try {
        const jsonValue = JSON.stringify(value)
        await AsyncStorage.setItem('user', jsonValue)
      } catch (e) {
        console.log(e)
      }
    }

    if (reply.success && reply.data.token) {
      store.setState({
        token: reply.data.token,
        user: reply.data.user,
        isLoggedIn: true,
      })
      storeUser({ ...reply.data.user, password })
    }
    return reply
  },
  googleLogin: async(store, user, token) => {
    const reply = await store.actions.requestAPI('POST', '/google', { user, token })

    if(reply.success && reply.data.token) {
      store.setState({
        token: reply.data.token,
        user: reply.data.user,
        isLoggedIn: true, 
        google: true
      })
    }
    const storeUser = async (value) => {
      try {
        const jsonValue = JSON.stringify(value)
        await AsyncStorage.setItem('user', jsonValue)
      } catch (e) {
        console.log(e)
      }
    }

    if (reply.success && reply.data.token) {
      store.setState({
        token: reply.data.token,
        user: reply.data.user,
        isLoggedIn: true,
      })
      storeUser({ ...reply.data.user, password })
    }
    return reply
  },
  register: async(store, email, password, name) => {
    const reply = await store.actions.requestAPI('POST', '/register', { email, password, name })
    if (reply.success && reply.data.exists !== true && reply.data.token) {
      store.setState({
        token: reply.data.token,
        user: reply.data.user,
        isLoggedIn: true,
        google: true
      })
    }
    return reply
  },
}

export default appActions