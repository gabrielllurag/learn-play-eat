import React from 'react'
import globalHook from './globalHook'
import actions from './actions'

const initialState = {
  user: false,
  progress: [],
  token: false,
  groupId: '',
  showWelcome: false,
  isSubscribed: false,
  isLoading: false,
  message: '',

  experiments: [],
  experiment: false,

  categories: [],
  category: false,

  activities: [],
  activity: false,

  food: false,
  foods: [],
  foodOrders: [],

  selectedFoods: [],
  currentFood: false,
  currentFoodIndex: 0,

  userTrieds: [],
  trieds: [],
  userSelects: [],
  score: 0,
  totalScore: 0,

  resetScene: false,

  level: 1,
  isLoggedIn: false,

  facebook: false,
  google: false,
  apple: false,

  onboarded: false,

  helpItems: [],
  pushTokens: '',
  allowPush: false,
  allowFCM: false,

  photos: [],
  notification: {
    display: false
  }
}

const useGlobal = globalHook(React, initialState, actions)
export default useGlobal
