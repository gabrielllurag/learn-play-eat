import React, { useState } from 'react'
import { TView, CButton, SView, ThemedText, Input } from '../../components'
import useGlobal from '../../hooks/useGlobal'

export default function CreateAccount({navigation}) {
  const [state, actions] = useGlobal(['user'])
  const [user, setUser] = useState({ email: '', password: '', name: '' })

  const handleCreateAccount = async() => {
    const reply = await actions.register(user.email, user.password, user.name)

    if (user.email.length == "") {
      alert("Email is required")
    } else if (user.password.length == "") {
      alert('Password is required')
    } else if (user.name.length == "") {
      alert("Parent name is required")
    } else if(!reply.success){
      alert("Already have an account")
    } else if (reply.success) {
      navigation.replace("TabScreen")
    }
  }

  return (
    <SView ph={10}>
      <TView ph={10}>
        <ThemedText italic mt={10} align="center">
          "After using the Learn Play Eat app, my son is now less scared of new foods and even eating wider variety"
        </ThemedText>
      <ThemedText
        italic
        align="center"
        mt={10}
        mb={10}>
        Julie, Mum of picky eater, Sydney
      </ThemedText>
      </TView>
      <Input
        placeholder="* Email"
        keyboardType="email-address"
        value={user.email}
        onChangeText={email => setUser({ ...user, email })}/>
      <Input
        placeholder="* Password"
        value={user.password}
        secureTextEntry
        onChangeText={password => setUser({ ...user, password })}/>
      <Input
        placeholder="* Parent's Name"
        value={user.name}
        onChangeText={name => setUser({ ...user, name })}/>
      <CButton title="Create Account" onPress={handleCreateAccount}/>
      <CButton text title="Login to my account" onPress={() => navigation.navigate('SignIn')} />
    </SView>
  )
}
