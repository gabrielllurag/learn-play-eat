import React, {useEffect} from 'react'
import { Platform } from 'react-native'
import { TView, SView, ThemedText, CButton } from '../../components'
import useGlobal from '../../hooks/useGlobal'
import * as AppleAuthentication from 'expo-apple-authentication';
import * as GoogleSignIn from 'expo-google-sign-in';

const AppleButton = ({ onPress }) => (
  <AppleAuthentication.AppleAuthenticationButton
    buttonType={AppleAuthentication.AppleAuthenticationButtonType.SIGN_IN}
    buttonStyle={AppleAuthentication.AppleAuthenticationButtonStyle.BLACK}
    cornerRadius={3}
    style={{ padding: 20 }}
    onPress={onPress}
  />
)

export default function SignIn({ navigation }) {
  const [state, actions] = useGlobal(['user', 'isLoggedIn', 'token'])

  useEffect(() => {
    initAsync()
  }, [])

  const initAsync = async () => {
    await GoogleSignIn.initAsync({
      clientId: '111647297102-sfvro400aqfaii5qhlls476ej65mj8lv.apps.googleusercontent.com',
    });
  };

  // for native
  const handleGoogleSignIn = async () => {
    try {
      await GoogleSignIn.askForPlayServicesAsync();
      const { type, user } = await GoogleSignIn.signInAsync();
      if (type === 'success') {
        actions.googleLogin(user, user.auth.accessToken)
        navigation.replace("TabScreen")
      }
    } catch ({ message }) {
      alert('login: Error:' + message);
    }
  };

  // googleSignin for expo only
  // const handleGoogleSignIn = () => {
  //   const config = {
  //     iosClientId: `957054907684-l766pgit1dcu0l01bfvk8k4c334638ml.apps.googleusercontent.com`,
  //     scopes: ['profile', 'email ']
  //   }
  //
  //   Google
  //     .logInAsync(config)
  //     .then((result) => {
  //       const { type, user, accessToken } = result
  //
  //       if (type == 'success') {
  //         actions.googleLogin(result.user, result.accessToken)
  //         navigation.navigate('CreateAccount')
  //       } else {
  //         alert('Failed to login')
  //       }
  //     })
  //     .catch(error => {
  //       console.log(error)
  //     })
  // }

  const handleAppleSignIn = async() => {
    try {
      const credential = await AppleAuthentication.signInAsync({
        requestedScopes: [
          AppleAuthentication.AppleAuthenticationScope.FULL_NAME,
          AppleAuthentication.AppleAuthenticationScope.EMAIL,
        ],
      });
      console.log("CREDENTIAL", credential)
      navigation.navigate("TabScreen")
      actions.appleLogin(credential)
    } catch (e) {
      if (e.code === 'ERR_CANCELED') {
        console.log(e)
      } else {
        console.log(e)
      }
    }
  }

  return (
    <SView ph={20}>
      <ThemedText italic mt={10} align="center">
        "After using the Learn Play Eat app, my son is now less scared of new foods and even eating wider variety"
      </ThemedText>
      <ThemedText italic align="center" mt={10}>Julie, Mum of picky eater, Sydney</ThemedText>
      <TView mv={20}>
        {Platform.OS === 'ios' ? <AppleButton onPress={handleAppleSignIn}/> : null}
        <CButton mt={10} title="Google Sign In" onPress={handleGoogleSignIn} />
      </TView>
      <TView row mv={20} mh={50} style={{ justifyContent: 'space-between' }}>
        <ThemedText bold>___________</ThemedText>
        <ThemedText italic>Or</ThemedText>
        <ThemedText bold>___________</ThemedText>
        </TView>
      <TView mt={20}>
        <CButton title="Create Account" onPress={() => navigation.navigate('CreateAccount')} />
        <CButton text mt={10} title="Login to my account" onPress={() => navigation.navigate('SignIn')}/>
      </TView>
    </SView>
  )
}
