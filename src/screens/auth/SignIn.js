import React, { useState, useEffect } from 'react'
import { ActivityIndicator } from 'react-native'
import { TView, CButton, SView, Input, ThemedText } from '../../components'
import useGlobal from '../../hooks/useGlobal'
import AsyncStorage from '@react-native-async-storage/async-storage'

export default function SignIn({ navigation }) {
  const [state, actions] = useGlobal(['user', 'isLoggedIn', 'token'])
  const [user, setUser] = useState({ email: '', password: '' })
  const [loading, isLoading] = useState(false)
  const [success, isSuccess] = useState(false)

  useEffect(() => {
    getUser()
  }, [])

  const handleLogin = async() => {
    isLoading(true)
    const reply = await actions.login(user.email, user.password)
    if (reply.success) {
      isLoading(false)
      navigation.replace('TabScreen')
    } else {
      isLoading(false)
      alert('Failed to log in')
    }
  }

  const getUser = async() => {
    isLoading(true)
    const item = await AsyncStorage.getItem('user')
    const progress = await AsyncStorage.getItem('progress')
    const items = JSON.parse(item)
    const progresses = JSON.parse(progress)

    if(items != null) {
      setUser({ email: items.email, password: items.password })
      await actions.login(items.email, items.password)
      isLoading(false)
      navigation.replace('TabScreen')
    } else {
      isLoading(false)
    }

    return items
  }

  return (
    <SView ph={10}>
      <TView ph={10}>
        <ThemedText italic mt={10} align="center">
          "After using the Learn Play Eat app, my son is now less scared of new foods and even eating wider variety"
        </ThemedText>
        <ThemedText
          italic
          align="center"
          mt={10}
          mb={10}>
          Julie, Mum of picky eater, Sydney
        </ThemedText>
      </TView>
      <Input
        placeholder="Email"
        keyboardType="email-address"
        value={user.email}
        onChangeText={email => setUser({ ...user, email })}
        />
      <Input
        placeholder="Password"
        value={user.password}
        secureTextEntry
        onChangeText={password => setUser({ ...user, password })}
        />
      <CButton title="Login" onPress={handleLogin} />
      <CButton
        text
        title="Create Account"
        onPress={() => navigation.navigate('CreateAccount')}
      />
      <TView hcenter mh={10}>
        <ActivityIndicator animating={loading} color="gray" size="large" />
      </TView>
    </SView>
  )
}
