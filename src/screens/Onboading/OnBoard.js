import React, { useEffect } from 'react';
import { Image, LogBox } from 'react-native';
import Swiper from 'react-native-swiper';
import { TView, CButton, ThemedText } from '../../components';
import AsyncStorage from '@react-native-async-storage/async-storage'

const Slide = ({ image, head, p }) => (
    <TView vcenter hcenter flex mh={20}>
        <Image style={{ height: 200, width: 200 }} source={image} />
        <ThemedText mt={10} size="h6">{head}</ThemedText>
        <ThemedText mt={20} style={{ textAlign: 'center' }}>{p}</ThemedText>
    </TView>
)

export default function OnBoard({ navigation }) {

    useEffect(() => {
        handleCheck()
        LogBox.ignoreLogs(['Require cycle:'])
    }, [])

    const handleCheck = async() => {
        try {
            const item = await AsyncStorage.getItem('user')
            const user = JSON.parse(item)
            if(user != null) {
                navigation.navigate("SignIn")
            }
        } catch(e) {
            console.log(e)
        }
    }

    return (
        <TView flex>
            <Swiper showsButtons={false} loop={false} index={0}>
                <Slide
                    head="Help for picky eaters"
                    p="Give your little eater the chance to interact with new foods through fun activities and games. No stress to eat, we learn and play with food, developing along the way."
                    image={require('../../../assets/onboard/onboard.png')} />
                <Slide
                    head="Whenever it suits you"
                    p="We'll guide you through the simple steps, do as much as you have time for, whenever suits you. You can also view how your child has progressed!"
                    image={require('../../../assets/onboard/onboard_2.png')} />
                <Slide
                    head="Designed by parents"
                    p="We know that small steps forwards can sometimes mean big wins! We know how tough it can be and we know how important results are."
                    image={require('../../../assets/onboard/onboard_3.png')} />
                <Slide
                    head="Approved by professionals"
                    p="Our app was designed with advice from feeding therapists and the approach is based on techniques used in clinics."
                    image={require('../../../assets/onboard/onboard.png')} />
            </Swiper>
            <TView mh={10} mb={20}>
                <CButton title="Get Started" onPress={() => navigation.navigate('SignUp')} />
            </TView>
        </TView>
    )
}
