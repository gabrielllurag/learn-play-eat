import React, { useState } from 'react';
import { FlatList, ActivityIndicator } from 'react-native';
import { TView, Input, CButton, ThemedText } from '../../components'
import useGlobal from '../../hooks/useGlobal'

const InviteCard = ({ invite, onPress }) => (
  <TView 
    style={{ 
      justifyContent: 'center', 
      alignItems: 'center', 
      flexDirection: 'row', 
      height: 40, 
      paddingHorizontal: 8, 
      borderBottomWidth: 1, 
      borderBottomColor: '#eee' 
    }}>
    <Text style={{ flex: 1, fontSize: 16 }}>{invite.email}</Text>
    {
      invite.is_credited
      ? <Text style={{ fontSize: 16 }}>Credited</Text>
      : 
      <TouchableOpacity 
        onPress={onPress} 
        style={{ 
          backgroundColor: '#50B7E4', 
          paddingVertical: 4, 
          paddingHorizontal: 10, 
          borderRadius: 4}}>
        <Text style={{ fontSize: 16, color: 'white' }}>
          Claim
        </Text>
      </TouchableOpacity>
    }
  </TView>
)

export default function Invitations() {
  const [state, actions] = useGlobal(['user'])
  const [email, setEmail] = useState('')
  const [sending, isSending] = useState(false)

  console.log("USER", state.user)

  const sendInvitation = async() => {
    isSending(true)

    if (email.match(/^[^\s@]+@[^\s@]+\.[^\s@]+$/ig)) {
      await actions.sendInvitation(email)
      setEmail('')
    } else {
      alert('Please enter a correct email address')
    }
    isSending(false)
  }

  const claimCredits = async(invite) => {
    await actions.claimCredits(invite.email)
  }

  return (
    <TView flex p={20}>
      <ThemedText bold align="center" mb={10}> Invite Your Friends</ThemedText>
      <ThemedText mb={10}>
        You can get 1 month FREE access for every friend who signs up that you invited
      </ThemedText>
      <Input placeholder="Email" autoCapitalize={'none'} onChangeText={email => setEmail(email)} />
      {
        sending
        ?
        <ActivityIndicator animating={sending} color="gray" size="large" />
        :
        <CButton title="Send Invite" onPress={() => sendInvitation()}/>
      }
      <FlatList
        data={state.user.invitations}
        keyExtractor={(item) => `invite-${item.id}`}
        renderItem={
          ({ item }) => 
            <InviteCard key={item.id} invite={item} onPress={() => this.claimCredits(item)} />
          }
        style={{ flex: 1 }}
      />
    </TView>
  )
}
