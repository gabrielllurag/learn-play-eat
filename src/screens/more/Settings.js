import React from 'react';
import { Linking } from 'react-native'
import { TView, CButton, ThemedText } from '../../components'

export default function Settings() {
  return (
    <TView flex p={20}>
      <ThemedText bold align="center" mb={10}>Settings</ThemedText>
      <ThemedText mb={10}>
        Open the Settings > Notifications > Learn Play Eat > Allow Notifications
      </ThemedText>
      <CButton title="Open app Settings" onPress={() => Linking.openSettings()}/>
    </TView>
  )
}
