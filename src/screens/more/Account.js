import React, { useState } from 'react';
import { ActivityIndicator } from 'react-native';
import { SView, ThemedText, Input, CButton } from '../../components'
import useGlobal from '../../hooks/useGlobal'

export default function Account({ navigation }) {
  const [state, actions] = useGlobal(['user'])
  const [password, setPassword] = useState('')
  const [currentPassword, setCurrentPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')
  const [loading, isLoading] = useState(false)

  const handleLogout = () => {
    isLoading(true)
    actions.logout()
    navigation.replace("OnBoarding")
  }

  return (
    <SView p={10}>
      {loading ? <ActivityIndicator animating={loading} color="gray" size="large" /> : null}
      <ThemedText mv={10} size="md" align="center" bold>Account Details</ThemedText>
      <Input value={state.user.email} />
      <Input value={state.user.name} />
      <ThemedText mv={10} size="md" align="center" bold>Change Password</ThemedText>
      <Input placeholder="Current Password" value={currentPassword} />
      <Input placeholder="New Password" value={password} />
      <Input placeholder="Confirm new password" value={confirmPassword} />
      <CButton title="Change Password" />
      <CButton title="Logout" onPress={handleLogout}/>
    </SView>
  )
}
