/* @flow */

import React from 'react';
import { Linking } from 'react-native'
import { TView, ThemedText, SView } from '../../components'
import { MaterialIcons } from '@expo/vector-icons';

const MoreListItem = ({ onPress, title }) => (
  <TView
    row
    pressable
    onPress={onPress}
    bgcolor="white"
    ph={15}
    pv={20}
    style={{ borderWidth: .3, borderColor: '#d3d3d3' }} 
    vcenter>
    <TView flex>
      <ThemedText size="sm">{title}</ThemedText>
    </TView>
    <MaterialIcons name="arrow-forward-ios" size={20}/>
  </TView>
)

export default function More({ navigation }) {
  return (
    <SView>
      <TView>
        <MoreListItem
          title="Account"
          onPress={() => navigation.navigate('Account')}
        />
        <MoreListItem
          title="Invitations and Free Credits"
          onPress={() => navigation.navigate('Invitations')}
        />
        <MoreListItem
          title="Settings"
          onPress={() => navigation.navigate('Settings')}
        />
        <ThemedText p={10}>More Support for picky eating</ThemedText>
        <MoreListItem
          title="Facebook Page"
          onPress={() => Linking.openURL('https://www.facebook.com/learnplayeat')}
        />
        <MoreListItem
          title="Facebook support group"
          onPress={() => Linking.openURL('https://www.facebook.com/groups/606428426548893')}
        />
        <MoreListItem
          title="Instagram"
          onPress={() => Linking.openURL('https://www.instagram.com/learnplayeat')}
        />
        <ThemedText p={10}>Legal</ThemedText>
        <MoreListItem
          title="Terms of use"
          onPress={() => Linking.openURL('http://learnplayeat.com/terms-of-use')}
        />
        <MoreListItem
          title="Privacy Policy"
          onPress={() => Linking.openURL('http://www.learnplayeat.com/privacy-policy')}
        />
      </TView>
    </SView>
  )
}
