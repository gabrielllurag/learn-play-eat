import React, { useState, useEffect, useCallback } from 'react'
import { ImageBackground, Image, Modal, FlatList, Button, ActivityIndicator, Alert } from 'react-native'
import { TView, ThemedText, CButton } from '../../components'
import { Camera, CameraPreview } from 'expo-camera';
import useGlobal from '../../hooks/useGlobal'
import * as ImagePicker from 'expo-image-picker'
import { CLOUDINARY_KEY, CLOUDINARY_SECRET, CLOUDINARY_UPLOAD_URL } from '../../hooks/config'
import moment from 'moment'
import sha1 from 'crypto-js/sha1'

const ListItem = ({ onPress, item }) => (
  <TView pressable onPress={onPress} vcenter hcenter m={20}>
    <Image source={item.image} resizeMode="contain" style={{ height: 70, width: 60 }}/>
    <TView row>
      <ThemedText>{item.category}. </ThemedText>
      <ThemedText>{item.name}</ThemedText>
    </TView>
  </TView>
)

const sensory = [
  { name: "Looking activities", category: 1, image: require('../../../assets/images/senseicons/sight.png'), message: "Let's use our eyes" },
  { name: "Smelling activities", category: 2, image: require('../../../assets/images/senseicons/smell.png'), message: "Let's use our nose" },
  { name: "Listening activities", category: 3, image: require('../../../assets/images/senseicons/listen.png'), message: "Let's use our ears" },
  { name: "Touching activities", category: 4, image: require('../../../assets/images/senseicons/touch.png'), message: "Let's use our hands" },
  { name: "Tasting activities", category: 5, image: require('../../../assets/images/senseicons/taste.png'), message: "Let's use our mouths" },
]

const games = [
  { name: "Learner Games", category: 6, image: require('../../../assets/images/senseicons/learnergames.png') },
  { name: "Expert Games", category: 7, image: require('../../../assets/images/senseicons/expertgames.png') }
]

export default function Detail({ navigation }) {
  const [state, actions] = useGlobal(['experiment', 'activities', 'photos'])
  const [camera, setCamera] = useState(false)
  const [selected, setSelected] = useState({sensory, camera})
  const [hasPermission, setHasPermission] = useState(null);
  const [type, setType] = useState(Camera.Constants.Type.front);
  const [snap, setSnap] = useState([])
  const [visible, isVisible] = useState(false)
  const [image, setImage] = useState([])
  const [currentFood, setCurrentFood] = useState([])
  const [loading, isLoading] = useState(false)
  const [uploading, isUploading] = useState(false)

  const cameraOptions = {
    title: 'Take a picture for the experiment',
    customButtons: [],
    allowsEditing: true,
    mediaType: ImagePicker.MediaTypeOptions.All,
    maxWidth: 400,
    maxHeight: 600,
    quality: 0.7,
    storageOptions: {
      skipBackup: true,
      path: 'image/lpe'
    }
  }

  const wait = (timeout) => {
    return new Promise(resolve => setTimeout(resolve, timeout));
  }

  const onRefresh = useCallback(async() => {
    isLoading(true)
    wait(1000).then(() => isLoading(false))
  }, [])

  useEffect(() => {
    navigation.setOptions({
      title: state.experiment.name,
      headerRight: () => <CButton text title="Done" onPress={handleConfirm}/>
    }),
    (async () => {
      const { cameraStatus } = await Camera.requestPermissionsAsync();
      const { imagePickerStatus } = await ImagePicker.requestMediaLibraryPermissionsAsync();
      setHasPermission(cameraStatus === 'granted');
    })();
    setSelected(sensory, setCamera(false))
    getCurrentFood(state.foods)
  }, [])

  const handleConfirm = () => {
    Alert.alert(
      "Find the Treasure",
      "Do you want to leave now?",
      [
        {
          text: "Yes",
          onPress: () => handleNavigation(state.foods)
        },
        {
          text: "Cancel",
          style: "cancel"
        }
      ]
    )
  }

  const handleNavigation = async(item) => {
    try {
      isLoading(true)
      const filtered = await item.filter(f => f.selected == true)
      const current = await filtered.filter(c => c.finished == false)
      console.log(current)
      const trieds = { tried: state.userTrieds, id: current[0].id }
      await actions.addToTrieds(trieds)
      
      if(current.length == 1) {
        isLoading(false)
        navigation.navigate("Finished")
      } else {
        isLoading(false)
        navigation.navigate("FinishedFood")
      }
    } catch(e) {
      console.log(e)
    }
  }

  const getCurrentFood = (item) => {
    let filtered = item.filter(f => f.selected == true)
    let current = filtered.filter(c => c.finished != true)
    setCurrentFood(current[0])
    actions.setCurrentFood(current[0])
    onRefresh()
  }

  const onChooseItem = (item) => {
    actions.setActivity(state.activities[item.category])
    getCurrentFood(state.foods)
    navigation.navigate('Activities', { data: item })
  }

  const pickImage = async() => {
    isUploading(true)
    let result = await ImagePicker.launchImageLibraryAsync(cameraOptions);

    if (!result.cancelled) {
      setImage(result)
    }
    let image = await cloudinaryUpload(result.uri)
    isUploading(false)
    await actions.getPhotos(image.secure_url)
  }

  async function cloudinaryUpload( uri = '', folder = 'experiments' ) {
    var timestamp = moment().format('X').toString();
    var keys = 'folder=' + folder + '&public_id=' + timestamp + '&timestamp=' + timestamp + CLOUDINARY_SECRET;
    var signature = sha1(keys).toString()
    const formdata = new FormData();
    // identify extension
    let ext = uri.split('.').pop();
    let extensions = {
      'jpg': 'image/jpg',
      'png': 'image/png'
    }
    // set form data
    formdata.append('file', {uri: uri, type: extensions[ext], name: `upload.${ext}`});
    formdata.append('public_id', timestamp);
    formdata.append('folder', folder);
    formdata.append('timestamp', timestamp);
    formdata.append('api_key', CLOUDINARY_KEY);
    formdata.append('signature', signature);

    try {
      let response = await fetch(CLOUDINARY_UPLOAD_URL + 'image/upload', {
        method: 'POST',
        body: formdata
      })
      return await response.json()
    } catch (e) {
      console.log('REQUEST_FAILURE', e)
      return { status: false, error: e }
    }
  }

  const __takePicture = async () => {
    isUploading(true)
    if (!snap) return
    isVisible(false)
    const photo = await snap.takePictureAsync()
    let image = await cloudinaryUpload(photo.uri)
    actions.getPhotos(image.secure_url)
    isUploading(false)
  }

  return (
    <TView flex>
      <TView>
        <ImageBackground
          style={{ height: 150, flexDirection: 'row' }}
          source={require('../../../assets/background.png')}>
          <TView mr={100}>
            <Image
              source={require('../../../assets/images/monkey_banana.png')}
              style={{ height: 80, width: 80, position: 'absolute', bottom: 1, left: 10 }} />
          </TView>
          <ImageBackground
            style={{ height: 130, width: 150, justifyContent: 'center' }}
            source={require('../../../assets/images/speechbubble.png')}>
          <ThemedText
            align='center'
            p={10}
            size='xs'>
            Tap below to get activities for exploring {state.experiment.name}
          </ThemedText>
          </ImageBackground>
        </ImageBackground>
        <TView
          bgcolor='green'
          style={{ height: 40, positon: 'absolute', bottom: 5 }}>
          {loading ? <ActivityIndicator animating={loading} color="white" size="large" /> : null}
        </TView>
      </TView>
      <TView row style={{ justifyContent: 'space-between' }}>
        <CButton
          style={{ width: '36%' }}
          onPress={() => setSelected(sensory, setCamera(false))}
          title="Sensory Activities" />
        <CButton
          style={{ width: '31%' }}
          onPress={() => setSelected(games, setCamera(false))}
          title="Play Games" />
        <CButton
          style={{ width: '30%' }}
          onPress={() => setCamera(true)}
          title="Camera" />
      </TView>
      {
        camera ?
        <TView ph={10}>
          <Modal animationType="slide" visible={visible} style={{ flex: 1 }}>
            <Camera
              ref={(r) => setSnap(r)}
              style={{ flex: 1, padding: 10 }}
              type={Camera.Constants.Type.front}>
              <TView flex pt={30} style={{ alignItems: 'flex-end' }}>
                <CButton
                  text
                  textSize="md"
                  title="Close"
                  onPress={() => isVisible(!visible)} />
              </TView>
              <TView flex style={{ alignItems: 'center', justifyContent: 'flex-end' }}>
                <Button
                  title=""
                  style={{ height: 50, width: 50, borderRadius: 25 }}
                  onPress={__takePicture}/>
              </TView>
            </Camera>
          </Modal>
          {uploading ? <ActivityIndicator animating={uploading} color="gray" size="large" /> : null}
          <TView style={{ flexWrap: 'wrap', flexDirection: 'row' }}>
          {
            state.photos.length > 0
            ?
            (
              state.photos.map((img, index) =>
              <Image
                key={index}
                source={{ uri: img }}
                style={{ height: 100, width: 100, margin: 5 }} />
              )
            )
            :
            null
          }
          </TView>
          <TView style={{ marginBottom: 100 }}>
            <CButton title="Open Gallery" onPress={pickImage} />
            <CButton title="Open Camera" onPress={() => isVisible(!visible)}/>
          </TView>
        </TView>
        :
        <FlatList
          numColumns={2}
          contentContainerStyle={{ alignItems: 'center', justifyContent: 'space-between' }}
          showsVerticalScrollIndicator={false}
          data={selected}
          renderItem={({ item }) => (
            <ListItem
              item={item}
              onPress={() => onChooseItem(item)} />
          )}
          keyExtractor={(item) => `item-${item.category}`}
        />
      }
    </TView>
  )
}
