import React, { useState, useRef, useEffect } from 'react'
import { ImageBackground, Image } from 'react-native'
import { WebView } from 'react-native-webview'
import { TView, CButton, ThemedText } from '../../components'
import { Video } from 'expo-av'
import useGlobal from '../../hooks/useGlobal'
import { VIDEO_URL } from '../../hooks/config'

export default function Info({ navigation }) {
  const [state, actions] = useGlobal(['experiment'])
  const video = useRef(null);
  const [status, setStatus] = useState({});
  const [placeholder, setPlaceholder] = useState(false)

  const vidPlaceholder = 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4'

  useEffect(() => {
    const groupId = Math.random().toString(36).substring(2, 8)
    actions.setGroupID(groupId)
    getVideo(state.experiment.video_url)
    navigation.setOptions({
      title: state.experiment.name,
      headerRight: () => <CButton text title="Next" onPress={() => { navigation.navigate('Detail') }}/>
    })
  }, []);

  const getVideo = async(video) => {
    const videoItem = await fetch(`http://35.208.112.63/videos/${video}`)
    if(videoItem.ok) {
      setPlaceholder(true)
    } else {
      setPlaceholder(false)
    }
  }

  return (
    <TView flex>
      <TView>
        <ImageBackground
          style={{ height: 150, flexDirection: 'row' }}
          source={require('../../../assets/background.png')}>
          <TView mr={100}>
            <Image
              source={require('../../../assets/images/monkey_banana.png')}
              style={{
                height: 80,
                width: 80,
                position: 'absolute',
                bottom: 1,
                left: 10
              }}
            />
          </TView>
          <ImageBackground
            style={{ height: 130, width: 150, justifyContent: 'center' }}
            source={require('../../../assets/images/speechbubble.png')}>
            <ThemedText
              align='center'
              p={10}
              size='xs'>
              Watch the video to learn more about {state.experiment.name}
            </ThemedText>
          </ImageBackground>
          <Image
            source={{ url: state.experiment.icon_url }}
            style={{ height: 100, width: 100, alignSelf: 'center' }} />
        </ImageBackground>
        <TView bgcolor='green' style={{ height: 40, positon: 'absolute', bottom: 5 }}></TView>
      </TView>
      <TView flex>
        { 
        placeholder ?
        <Video
          ref={video}
          style={{ height: 200 }}
          source={{ uri: VIDEO_URL + '/videos/' + state.experiment.video_url }}
          useNativeControls
          resizeMode={Video.RESIZE_MODE_CONTAIN}
          isLooping
          onPlaybackStatusUpdate={status => setStatus(() => status)}
         />
         :
        <TView p={10} style={{ height: 200 }}>
          <WebView
            javaScriptEnabled={true}
            domStorageEnabled={true}
            source={{uri: 'https://youtu.be/kWK2GeYwg-M' }}
          />
        </TView>
        }
      </TView>
    </TView>
  )
}
