import React, { useState, useEffect } from 'react'
import { ImageBackground, Linking, Image, ActivityIndicator, Platform } from 'react-native'
import { CButton, IconButton, SView, ThemedText } from '../../components'
import useGlobal from '../../hooks/useGlobal'
import {
  connectAsync,
  disconnectAsync,
  finishTransactionAsync,
  getBillingResponseCodeAsync,
  getProductsAsync,
  getPurchaseHistoryAsync,
  IAPResponseCode,
  purchaseItemAsync,
  setPurchaseListener,
} from 'expo-in-app-purchases';
import moment from 'moment'

export default function Subscribe({ navigation }) {
  const [state, actions] = useGlobal(['user'])
  const [products, setProducts] = useState([])
  const [canPurchase, isCanPurchase] = useState(true)
  const [loading, isLoading] = useState(false)
  const [history, setHistory] = useState([])
  const [billing, setBilling] = useState(false)
  const [subscribed, isSubscribed] = useState(false)

  useEffect(() => {
    navigation.setOptions({
      title: "Welcome",
      headerLeft: () => <IconButton icon="arrow-back" size={25} onPress={() => handleCancel() }/>
    })
    handleChecking()
  }, [])

  const handleCancel = async() => {
    if(Platform.OS == 'android') {
      navigation.goBack()
    } else {
      await disconnectAsync()
      navigation.goBack()
    }
  }

  async function handleChecking() {
    try {
      const { user } = state

      await actions.saveSubs(JSON.stringify({
        purchases: history.map(pur => ({ ...pur, transactionReceipt: '' }))
      }))

      let appStoreExpiration = moment(user.subscription_end);
         let creditExpiry = user.credit_expiry;
         if (typeof creditExpiry == 'string') {
             creditExpiry = creditExpiry.substring(0, creditExpiry.length - 3);
         }

        let creditExpiration = moment.unix(creditExpiry);
        let now = moment();

        if (appStoreExpiration > now || creditExpiration > now) {
          actions.setSubscribed(true)
          setSubscribed(true)
          navigation.navigate("Food")
          return;
        }

        if (subscribed == false) {
            await connectAsync()
            await getPurchaseHistory()
            await getProducts()
            isCanPurchase(true)
            isLoading(false)
          } else {
            isCanPurchase(false)
            isLoading(false)
          }

        setPurchaseListener(({ responseCode, results, errorCode }) => {
          if (responseCode === IAPResponseCode.OK) {
            for (const purchase of results) {
              console.log(`Successfully purchased ${purchase.productId}`);
              if (!purchase.acknowledged) {
                finishTransactionAsync(purchase, true);
                getBillingResult()
                actions.setSubscribed(true)
                isLoading(false)
              }
            }
          } else if (responseCode === IAPResponseCode.USER_CANCELED) {
            console.log('User canceled');
            isLoading(false)
          } else {
            alert(`Something went wrong with the purchase. Received response code ${responseCode} and errorCode ${errorCode}`)
            isLoading(false)
          }
        });
    } catch (e) {
      alert(e)
    }
  }
  console.log("Products", products)
  console.log("History", history)

  async function getProducts() {
    const { responseCode, results } = await getProductsAsync(['monthly_subscription']);
    if (responseCode === IAPResponseCode.OK) {
      setProducts(results)
    }
  }

  async function restorePurchases() {
    try {
      const { user } = state
      isLoading(true)

      const sub = history.pop()

      let expiry = moment(history.transactionDate).add(1, 'month')
      let current = moment()
      let remainingDays = expiry.diff(current, 'days') + 1
      let allowPass = (history.length >= 1)

      await giveCreditsForOldUsers()

      if (remainingDays <= 0) {
        // check for user expiration
        if (parseInt(user.credit_expiry) <= parseInt(current.format('x'))) {
          // means credit expired, check if has available credits
          if (user.available_credits > 0) {
            // update subscription
            let expiration = moment().add(user.available_credits, 'months').format('x')
            await actions.setSubs(expiration, user.available_credits)
            allowPass = true
          } else {
            allowPass = false
            alert(`You don't have available credits for FREE subscription. You can get 1 month FREE access for every friend who signs up that you invited`)
          }
        } else {
          allowPass = true
        }
      } else {
        alert(`Your subscription is already expired for ${remainingDays} day(s)`)
      }

      if (allowPass) {
        navigation.navigate("Food")
      }

      isLoading(false)

    } catch (e) {
      // alert(e)
      alert(`There was an error restoring purchase. Please contact administrator ${e}`)
    }
  }

  async function giveCreditsForOldUsers() {
    const { user } = state
    if (history.length > 0) {
      const purchase = history.pop()

      console.log('purchase', purchase);
      if (user.total_credits < 6 && purchase) {
        if (purchase.transactionDate < 1572368400000) {
          alert('We are giving you FREE access for 6 months')
          let expiration = moment().add(6, 'months').format('x')
          await actions.setSubs(expiration, 6)
        }
      }
    }
  }

  async function purchase() {
    isLoading(true)
    await purchaseItemAsync(products[0].productId)
  }

  async function getBillingResult() {
    const responseCode = await getBillingResponseCodeAsync();
    setBilling(responseCode)
  }

  async function getPurchaseHistory() {
    isLoading(true)
    const results = await getPurchaseHistoryAsync(true);
    console.log(results)
    if (results.responseCode === IAPResponseCode.OK) {
      setHistory(results.results)
      isLoading(false)
    } else {
      console.log("FAILED", results.responseCode)
      isLoading(false)
    }
  }

  return (
    <ImageBackground style={{ flex: 1 }} source={require('../../../assets/images/appstoreback.png')}>
      <SView ph={10} mb={5}>
        <Image
          source={require('../../../assets/images/appstoretop.png')}
          style={{ height: 150, width: '100%' }}
        />
        <ThemedText
          size="md"
          bold
          mb={10}
          mt={15}>
          Helping Families overcome picky eating
        </ThemedText>
        <ThemedText size="md" mb={10}>
          • 200+ Food activities & games
          {"\n"}
          • More activities and foods added regularly
          {"\n"}
          • Track your child’s progress
          {"\n"}
          • Reminders to help you schedule sessions
          {"\n"}
          • Daily tips and mealtime hacks
          {"\n"}
          • Exclusive community support group
          {"\n"}
          • Approved by qualified feeding therapists
        </ThemedText>
        <CButton title="Continue" onPress={() => purchase()}/>
        <CButton title="Restore Purchases" onPress={() => restorePurchases()} mb={10}/>
        <ActivityIndicator animating={loading} size="large" color="gray" />
        <ThemedText
          size="sm"
          align="center"
          mb={10}>
          Subscription terms: After free trial, monthly subscription is $7.99 AUD ($4.99 USD). Subscriptions automatically renews unless cancelled. Can be cancelled at anytime.
        </ThemedText>
        <CButton
          text
          title="Terms of use"
          textColor="black"
          onPress={() => Linking.openURL('https://learnplayeat.com/terms/')}
        />
        <CButton
          text
          title="Privacy Policy"
          textColor="black"
          onPress={() => Linking.openURL('https://learnplayeat.com/privacy-policy')}
        />
      </SView>
    </ImageBackground>
  )
}
