import React, { useState, useEffect, useCallback } from 'react';
import { Image, ActivityIndicator } from 'react-native';
import { TView, CButton, ThemedText } from '../../components'
import useGlobal from '../../hooks/useGlobal'
import moment from 'moment'
import AsyncStorage from '@react-native-async-storage/async-storage'

export default function Finished({ navigation }) {
  const [state, actions] = useGlobal(['experiment', 'user'])
  const [loading, isLoading] = useState(false)

  useEffect(() => {
    navigation.setOptions({
      title: state.experiment.name,
      headerRight: () => <CButton text title="Done" onPress={() => handleFinish() }/>})
      actions.saveProgress()
      actions.reset()
  }, []);

  const wait = (timeout) => {
    return new Promise(resolve => setTimeout(resolve, timeout));
  }

  const handleFinish = useCallback(() => {
    isLoading(true)
    const created_at = moment().format('l, LTS')
    handleAddToProgress(
      state.progress,
      state.experiment, 
      state.foods, 
      state.groupId, 
      state.score,
      state.photos,
      created_at
    )
    wait(1000).then(() => isLoading(false))
    navigation.navigate('Welcome')
    navigation.navigate('Progress')
  }, [])

  const handleAddToProgress = async(progress, experiment, foods, groupId, score, photos, date) => {
    try {
      isLoading(true)
      const item = { ...experiment, foods, groupId, score, photos, created_at: date }
      const items = progress
      const newProgress = items.push(item)
      actions.addToProgress(newProgress)
      const value = JSON.stringify(progress)
      await AsyncStorage.setItem('progress', value)
      isLoading(false)
    } catch(e) {
      console.log(e)
    }
  }

  return (
    <TView withBackground flex p={10}>
      <TView card flex hcenter pt={30} bgcolor="white">
        <ThemedText size="h5" bold>Level {state.level}</ThemedText>
        <ThemedText size="h4" bold mt={20}>{state.experiment.name} Victory</ThemedText>
        <ThemedText mv={20} bold size="lg">Final Score</ThemedText>
        <TView vcenter hcenter>
          <TView style={{ position: "absolute" }}>
            <ThemedText size="h3">{state.score}</ThemedText>
          </TView>
          <Image
            resizeMode="contain"  
            source={require('../../../assets/images/trophy.png')}
            style={{ height: 180, width: 130 }} />
        </TView>
        <ThemedText
          size="lg"
          bold
          align="center"
          mt={20}>
          What a brave food explorer, you got to Level {state.level}
        </ThemedText>
        {loading ? <ActivityIndicator animating={loading} color="gray" size="large" /> : null}
      </TView>
    </TView>
  )
}
