import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

export default function Certificate() {
  return (
    <View style={styles.container}>
      <Text>I'm the Certificate component</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
