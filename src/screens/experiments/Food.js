import React, { useState, useEffect, useCallback } from 'react';
import { FlatList, ActivityIndicator } from 'react-native'
import { TView, ThemedText, CButton } from '../../components'
import useGlobal from '../../hooks/useGlobal'

const FoodItems = ({ onPress, item, position, selected }) => (
  <TView
    pressable
    row
    color="white"
    onPress={onPress}
    pv={20}
    ph={30}
    style={{ 
      borderBottomWidth: .5, 
      borderColor: 'gray', 
      justifyContent: 'space-between' 
    }} >
    <ThemedText size="lg">{item.name}</ThemedText>
    {
      item.selected
      ?
      <ThemedText bold color="#d3d3d3" size="md">
        {item.position}
      </ThemedText>
      :
      null
    }
  </TView>
)

export default function Food({ navigation }) {
  const [state, actions] = useGlobal(['experiments', 'experiment', 'foods', 'foodOrders', 'selectedFoods'])
  const [initialState, setInitialState] = useState({ experiment: state.experiment, foods: state.experiment.foods })
  const [selections, setSelections] = useState([])
  const [loading, isLoading] = useState(true)
  const [orders, setOrders] = useState(false)

  const wait = (timeout) => {
    return new Promise(resolve => setTimeout(resolve, timeout));
  }

  const Refresh = useCallback(async() => {
    isLoading(true)
    setInitFood(state.experiment.foods)
    wait(1000).then(() => isLoading(false))
  }, [])

  const setInitFood = async(items) => {
    isLoading(true)
    try {
      const foods = await items.map(item => {
        return { ...item, finished: false }
      })
      const filter = await initialState.foods.filter(f => f.selected == true)
      const selected = await filter.map(s => {
        return s.id
      })
      actions.setFoods(foods)
      isLoading(false)
      actions.setSelectedFoods(selected)
      setInitialState({foods})
      console.log("STATE", state.foods)
    } catch(e) {
      console.log(e)
    }
  }
  
  useEffect(() => {
    Refresh()
    actions.getActivities(state.experiment.activities)
    setOrders(state.foodOrders)
    actions.getUser()
    navigation.setOptions({
      title: state.experiment.name,
      headerRight: () => <CButton text title="Next" onPress={onNext}/>
    })
  }, []);

  const onNext = async() => {
    await isLoading(true)
    if (orders.length == 0) {
      alert("Choose atleast 1 food")
      isLoading(false)
    } else {
      isLoading(false)
      navigation.navigate("Info")
    }
  }

  function onSelectItem(foodId) {
    const exists = (selections.indexOf(foodId) > -1)

    if (exists) {
      const id = selections.indexOf(foodId)
      selections.splice(id, 1)
    } else {
      selections.push(foodId)
    }

    const foods = initialState.foods.map(food => {
      let position = selections.indexOf(food.id)
      if (position > -1) {
        return { ...food, position: position + 1, selected: true, finished: false }
      } else {
        return { ...food, position: '', selected: false, finished: false }
      }
    })

    actions.setFoods(foods)
    actions.setSelectedFoods(selections)
    setInitialState({ foods })
  }

  return (
    <TView withBackground flex pv={20} ph={20}>
      <ThemedText
        align="center"
        color="white"
        mb={10} >
        Select 3 or 4 foods to use, and the order which your child might like best
      </ThemedText>
      {loading ? <ActivityIndicator animating={loading} size="small" color="gray" /> : null}
      <TView card flex bgcolor="white" ph={10}>
        {
          initialState.foods.length > 0 ?
          <FlatList
            style={{ flex: 1 }}
            showsVerticalScrollIndicator={false}
            data={initialState.foods}
            renderItem={({ item }) => (
              <FoodItems onPress={() => onSelectItem(item.id)} item={item} />
            )}
            keyExtractor={(item) => `item-${item.id}`}
          />
        :
        <ActivityIndicator size="small" color="#0000ff" />
      }
      </TView>
    </TView>
  )
}
