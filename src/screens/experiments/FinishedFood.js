import React, { useState, useEffect } from 'react';
import { Image } from 'react-native';
import { TView, ThemedText, CButton } from '../../components'
import useGlobal from '../../hooks/useGlobal'

export default function FinishedFood({ navigation }) {
  const [state, actions] = useGlobal()
  const [finishing, setFinishing] = useState(false)
  const [currentFoods, setCurrentFoods] = useState(false)

  useEffect(() => {
    navigation.setOptions({
      title: state.experiment.name,
      headerRight: () => <CButton text title="Done" onPress={() => handleFinishedFood()}/>
    })
    filterCurrentFood()
  }, []);

  function handleFinishedFood() {
    try {
      setFinishing(true)
      let index = state.foods.findIndex(i => i.id == state.currentFood.id )
      let order = state.foods
  
      if (order) {
        order[index].finished = true
        actions.setSelectedFoods(order)
        actions.resetTrieds()
        setFinishing(false)
        navigation.replace('Detail')
      }
    } catch(e) {
      console.log(e)
    }
  }

  const filterCurrentFood = () => {
    const filtered = state.foods.filter(c => c.selected == true)
    const current = filtered.filter(c => c.finished == false)
    setCurrentFoods(current)
  }

  return (
    <TView withBackground p={10}>
      <TView card hcenter flex bgcolor="white" p={20}>
        <ThemedText size="lg" bold>Level {state.level}</ThemedText>
        <ThemedText size="lg" mt={20}>{state.currentFood.name} completed</ThemedText>
        <ThemedText mv={20} size="lg">Score {state.score}</ThemedText>
        <Image
          source={require('../../../assets/images/monkey_tongue.png')}
          style={{ height: 120, width: 125 }} />
        <ThemedText size="lg" mt={20}>Great Score!</ThemedText>
        <ThemedText size="lg" mt={10}>Can you beat the next food?</ThemedText>
      </TView>
    </TView>
  )
}
