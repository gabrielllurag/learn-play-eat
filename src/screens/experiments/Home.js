/* @flow */

import React, { useEffect, useState } from 'react';
import { ActivityIndicator, Image, FlatList } from 'react-native';
import { TView, ThemedText } from '../../components';
import useGlobal from '../../hooks/useGlobal'
import moment from 'moment'

const Items = ({item, onPress}) => (
  <TView flex pressable mb={10} onPress={onPress}>
    <TView bgcolor="#d3d3d3" card pv={16} ph={10} vcenter mb={10}>
      <TView ml={100}>
        <ThemedText size="md">{item.name}</ThemedText>
      </TView>
    </TView>
    <Image
      source={{uri: item.icon_url }}
      style={{
        height: 67,
        width: 67,
        position: 'absolute',
        left: 5
      }} />
  </TView>
)

function Home({ navigation }) {
  const [state, actions] = useGlobal(['experiments'])
  const [subscribed, isSubscribed] = useState(false)

  useEffect(() => {
    navigation.setOptions({ title: "Let's explore..." })
    actions.getExperiments()
  }, []);

  const onChoose = (id) => {
    let appStoreExpiration = state.user.subscription_end
    let creditExpiry = state.user.credit_expiry
    if (typeof creditExpiry == 'string') {
      creditExpiry = creditExpiry.substring(0, creditExpiry.length - 3)
    }

    let creditExpiration = moment.unix(creditExpiry)
    let now = moment()

    const experiment = state.experiments.find(e => e.id == id)
    actions.setExperiment(experiment)
    if (appStoreExpiration > now || creditExpiration > now) {
      navigation.navigate('Subscribe')
    } else {
      navigation.navigate('Food')
    }
  }

  console.log("USER", state.user)

  return (
    <TView withBackground pv={20} ph={20}>
      <TView mb={10}>
        <ThemedText color='white' align='center'>Choose group</ThemedText>
      </TView>
      <TView card flex bgcolor="white" pv={20} ph={20}>
        {
          state.experiments.length > 0 ?
          <FlatList
            showsVerticalScrollIndicator={false}
            data={state.experiments}
            renderItem={({ item }) => (
              <Items onPress={() => onChoose(item.id) } item={item} />
            )}
            keyExtractor={(item) => item.name}
          />
          :
          <ActivityIndicator size="small" color="#0000ff" />
        }
      </TView>
    </TView>
  )
}

export default Home;
