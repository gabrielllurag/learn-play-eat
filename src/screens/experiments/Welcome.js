
import React, { useState, useEffect } from 'react';
import { Modal } from 'react-native';
import useGlobal from '../../hooks/useGlobal'
import { TView, ThemedText, CButton } from '../../components';

function Welcome({ navigation }) {
  const [state, actions] = useGlobal(['user', 'experiments', 'helpItems'])
  const [tellVisible, isTellVisible] = useState(false)
  const [needVisible, isNeedVisible] = useState(false)

  useEffect(() => {
    actions.getExperiments();
    actions.getHelpItems();
    actions.getUser()
    actions.getProgress()
  }, []);
  
  return (
    <TView withBackground style={{ flex: 1, paddingHorizontal: 20, paddingVertical: 20 }}>
      <TView card bgcolor="white" flex ph={10} pv={20}>
        <ThemedText
          align='center'
          bold
          size='sm'>
          Welcome to Learn Play Eat
        </ThemedText>
        <ThemedText
          size='md'
          align='center'
          mt={20}>
          We'll suggest groups of playful activities to do with your child, helping them become more comfortable with food through learning and play.
        </ThemedText>
        <TView mt={20}>
          <CButton color='#808080' onPress={() => isTellVisible(true)} title="Tell me more" />
          <CButton color='#808080' onPress={() => isNeedVisible(true)} title="What will I need?" />
          <CButton title="Let's Go!" onPress={() => navigation.navigate("Home")}/>
        </TView>
      </TView>

      <Modal
        animationType="slide"
        transparent={true}
        visible={tellVisible}
        style={{ paddingHorizontal: 20 }}>
        <TView flex ph={20} vcenter>
          <TView card bgcolor='white' ph={20} pv={20}>
          <ThemedText mb={20} size='md'>Tell me more</ThemedText>
            <ThemedText>
              This app may be helpful for kids with sensory challenges or phobias towards new foods
              {"\n"}{"\n"}
              It is based on methods used by occupational therapists and other health professionals.
              {"\n"}{"\n"}
              Kids become more familiar with different foods, and reducing stress related to new foods overall.
              {"\n"}{"\n"}
              We have a system of points, levels, and certificates to make it feel more like a game
              {"\n"}{"\n"}
              The child is not asked by the parent to interact with the food. Rather, it is the monkey doing the asking!
              {"\n"}{"\n"}
              It can be done whenever it suits you, at a fraction of the cost of visiting a health professional.
              {"\n"}{"\n"}
              It’s not a quick fix. It is, however, a no-stress, fun way to interact with food, changing long-term mindsets and establishing healthy relationships with food.
            </ThemedText>
            <TView mt={20}>
              <CButton title="Close" onPress={() => isTellVisible(!tellVisible)} />
            </TView>
          </TView>
        </TView>
      </Modal>

      <Modal
        animationType="slide"
        transparent={true}
        visible={needVisible}
        style={{ paddingHorizontal: 20 }}>
        <TView flex ph={20} pb={40} vcenter>
          <TView card bgcolor="white" ph={20} pv={20}>
          <ThemedText>What will I need?</ThemedText>
            <ThemedText>
              {"\n"}{"\n"}
              • 3 to 4 foods for the experiment you want to try
              {"\n"}{"\n"}
              • Plates for you and your child
              {"\n"}{"\n"}
              • Tongs, spoons, and other serving equipment
              {"\n"}{"\n"}
              • Napkins
              {"\n"}{"\n"}
              • For some of the activities, you can choose to use a toy car, measuring tape or ruler, or a clean paint brush
              {"\n"}{"\n"}
              • Keep it all light-hearted and set aside 15 minutes when you won’t be disturbed
            </ThemedText>
            <TView mt={80}>
              <CButton title="Close" onPress={() => isNeedVisible(!needVisible)} />
            </TView>
          </TView>
        </TView>
      </Modal>
    </TView>
  )
}

export default Welcome;
