import React, { useState, useEffect } from 'react';
import { Image, ImageBackground, FlatList, ActivityIndicator } from 'react-native'
import { TView, CButton, ThemedText, IconButton } from '../../components'
import useGlobal from '../../hooks/useGlobal'

const ListItem = ({ onPress, checked, item }) => (
  <TView row hcenter pv={10} style={{ borderBottomWidth: .3, borderColor: '#d3d3d3' }}>
    {
      checked
      ?
      <IconButton onPress={onPress} size={40} icon="check-box" />
      :
      <IconButton onPress={onPress} size={40} icon="check-box-outline-blank" />
    }
    <TView flex>
      <ThemedText size="md" numberOfLines={2}>
        {item.title}
      </ThemedText>
    </TView>
  </TView>
)

export default function Activities({ navigation, route }) {
  const data = route.params
  const [state, actions] = useGlobal(['experiment', 'activity', 'score'])
  const [selections, setSelections] = useState([])
  const [items, setItems] = useState([])
  const [orders, setOrders] = useState([])
  const [loading, isLoading] = useState(false)
  
  const triedKeys = Object.keys(state.userTrieds)

  useEffect(() => {
    navigation.setOptions({
      title: state.experiment.name,
      headerLeft: () =>
        <CButton text title="Cancel" onPress={handleCancel}/>
      })
    setOrders(state.foods)
    loadTried(state.userTrieds)
  }, []);

  const handleCancel = () => {
    setSelections([])
    navigation.navigate("Detail")
  }

  const loadTried = (id) => {
    const filter = state.activity.filter(e => id.indexOf(e.id) !== -1)
    const ids = filter.map(i => {
      return i.id
    })

    const selected = ids
    setSelections(selected)
    const tried = state.activity.map(item => {
      let position = selected.indexOf(item.id)
      if (position > -1) {
        return { ...item, selected: true }
      } else {
        return { ...item, selected: false }
      }
    })
    setItems(tried)
  }

  const onChecked = (id) => {
    const exists = (selections.indexOf(id) > -1)

    if (exists) {
      const activity = selections.indexOf(id)
      selections.splice(activity, 1)
    } else {
      selections.push(id)
    }

    const data = items.map(item => {
      let position = selections.indexOf(item.id)
      if (position > -1) {
        return { ...item, selected: true }
      } else {
        return { ...item, selected: false }
      }
    })
    setItems(data)
    saveTried(id)
  }
  
  const saveTried = (id) => {
    const exists = (state.userTrieds.indexOf(id) > -1)

    if (exists) {
      const activity = state.userTrieds.indexOf(id)
      state.userTrieds.splice(activity, 1)
    } else {
      state.userTrieds.push(id)
    }
  }

  const onSaved = async() => {
    isLoading(true)
    let score = await items.filter(i => i.selected == true).length
    await actions.setScore(score)
    await actions.saveTried(state.currentFood, triedKeys)
    isLoading(false)
    navigation.navigate("Detail")
  }

  return (
    <TView flex>
      <ImageBackground
        style={{ height: 150, flexDirection: 'row' }}
        source={require('../../../assets/background.png')}>
        <TView mr={100}>
          <Image
            source={require('../../../assets/images/monkey_banana.png')}
            style={{
              height: 80,
              width: 80,
              position: 'absolute',
              bottom: 1,
              left: 10
            }}
          />
        </TView>
        <ImageBackground
          style={{ height: 130, width: 150, justifyContent: 'center' }}
          source={require('../../../assets/images/speechbubble.png')}>
          <ThemedText
            align='center'
            p={10}
            size='xs'>
            {data.data.message}! Tick off what you can do today.
          </ThemedText>
        </ImageBackground>
      </ImageBackground>
      <TView flex>
        <TView bgcolor='green' vcenter style={{ height: 40, positon: 'absolute', bottom: 5 }}>
          <ThemedText
            upper
            color="white"
            size="md"
            align="center">
            {data.data.name} For {state.currentFood.name}
          </ThemedText>
        </TView>
        {loading ? <ActivityIndicator animating={loading} color="gray" size="large" /> : null}
        <FlatList
          contentContainerStyle={{ paddingVertical: 10 }}
          showsVerticalScrollIndicator={false}
          data={items}
          renderItem={({ item }) => (
            <ListItem
              item={item}
              checked={item.selected}
              onPress={() => onChecked(item.id)} />
          )}
          keyExtractor={(item) => `item-${item.id}`}
        />
        <TView ph={10} style={{ justifyContent: 'flex-end' }}>
          <CButton title="Save Changes" onPress={() => onSaved()}/>
        </TView>
      </TView>
    </TView>
  )
}
