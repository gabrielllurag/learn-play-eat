import React, { useState, useEffect, useCallback } from 'react'
import { FlatList, Share, RefreshControl } from 'react-native'
import { TView, ThemedText, IconButton } from '../../components'
import useGlobal from '../../hooks/useGlobal'

const ProgressListItem = ({ onPress, title, onSharePress, onGalleryPress, date, score }) => (
  <TView
    row
    vcenter
    p={10}
    style={{
      justifyContent: 'space-between',
      borderBottomWidth: .3,
      borderColor: "#d3d3d3"
    }}>
    <TView pressable onPress={onPress} vcenter hcenter>
      <ThemedText bold>{title}</ThemedText>
      <ThemedText>{date}</ThemedText>
    </TView>
    <TView vcenter hcenter>
      <ThemedText>Score</ThemedText>
      <ThemedText>{score}</ThemedText>
    </TView>
    <TView row>
      <IconButton color="#d3d3d3" icon="apps" onPress={onGalleryPress} size={25}/>
      <IconButton color="#d3d3d3" icon="share" onPress={onSharePress} size={25}/>
    </TView>
  </TView>
)

export default function Progress({ navigation }) {
  const [state, actions] = useGlobal(['user', 'experiment'])
  const [loading, isLoading] = useState(false)
  const [refreshing, setRefreshing] = useState(false)
  const [progress, setProgress] = useState(state.progress)

  useEffect(() => {
    onRefresh()
  }, [])

  const wait = (timeout) => {
    return new Promise(resolve => setTimeout(resolve, timeout));
  }

  const sortItems = (sortBy) => {
    const item = sortBy.sort(function(a, b){
      return new Date(b.created_at) - new Date(a.created_at);
    })
    setProgress(item)
  }

  const onRefresh = useCallback(() => {
    setRefreshing(true)
    try {
      getProgress()
    } catch(e) {
      console.log(e)
    }
    wait(1000).then(() => setRefreshing(false))
  }, [])

  const getProgress = async() => {
    try {
      await actions.getProgress()
      isLoading(false)
    } catch(e) {
      console.log(e)
    }
  }

  const onShare = async() => {
    try {
      const result = await Share.share({
        title: 'Learn Play Eat',
        message: 'Share your progress!',
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  const handleNavigation = (item) => {
    isLoading(true)
    actions.setExperiment(item)
    onRefresh()
    isLoading(false)
    navigation.navigate("Food")
  }

  return (
    <TView withBackground pv={20} ph={20}>
      <TView card flex bgcolor="white" pv={10} ph={10}>
        <ThemedText
          bold
          size="md"
          mb={10}
          align="center">
          You are a level {state.user.level} Food Explorer
        </ThemedText>
        <ThemedText
          align="center"
          color="#e3843a">
          You can do the activities again or explore new foods
        </ThemedText>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={state.progress}
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={() => onRefresh()}
            />
          }
          renderItem={(item) =>
            <ProgressListItem
              onSharePress={onShare}
              onGalleryPress={() => navigation.navigate('Gallery', { item: item.item })}
              onPress={() => handleNavigation(item.item)}
              title={item.item.name} 
              date={item.item.created_at} 
              score={item.item.score}/>
          }
          keyExtractor={(item) => `item-${item.groupId}`}
        />
      </TView>
    </TView>
  )
}
