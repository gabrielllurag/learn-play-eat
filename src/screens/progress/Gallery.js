import React, { useState, useEffect, useCallback } from 'react';
import { Image, ActivityIndicator } from 'react-native'
import { TView, SView, ThemedText } from '../../components'
import useGlobal from '../../hooks/useGlobal'

export default function Gallery({ route }) {
  const data = route.params
  const [state, actions] = useGlobal(['experiments', 'user', 'photos'])
  const [progress, setProgress] = useState([])
  const [loading, isLoading] = useState(false)

  const wait = (timeout) => {
    return new Promise(resolve => setTimeout(resolve, timeout));
  }

  const onStart = useCallback(() => {
    isLoading(true)
    wait(1000).then(() => isLoading(false))
  }, [])

  useEffect(() => {
    onStart()
  }, [])

  const photos = data.item.photos
 
  return (
    <TView withBackground flex p={20}>
      <TView flex card bgcolor="white" p={10}>
      {loading ? <ActivityIndicator animating={loading} color="gray" size="small" /> : null}
      {
        photos.length > 0 ?
       <SView flex style={{ flexWrap: 'wrap', paddingTop: 10 }}>
         <TView row>
          { photos.map((img, index) => (
            <Image
              key={index}
              source={{ uri: img }}
              style={{
                height: 100,
                width: 100, 
                borderRadius: 10, 
                marginHorizontal: 8, 
                marginBottom: 10 
              }}
            />
          ))}
         </TView>
       </SView>
        :
        <TView flex vcenter hcenter>
          <Image
            source={require('../../../assets/images/monkey_face.png')}
            style={{ height: 50, width: 55, marginBottom: 20 }}
          />
          <ThemedText bold mb={20} p={5} size='md' align="center">
            No photos were taken for this experiment.
          </ThemedText>
          <ThemedText size="md" p={5} align="center">
            You can take photos of the activity by tapping the CAMERA tab on the Activity Screen
          </ThemedText>
        </TView>
      }
      </TView>
    </TView>
  )
}
