
import React from 'react'
import { FlatList } from 'react-native'
import { TView, ThemedText } from '../../components'
import useGlobal from '../../hooks/useGlobal'
import { MaterialIcons } from '@expo/vector-icons';

const HelpListItem = ({ item, onPress }) => (
  <TView
    row
    pressable
    onPress={onPress}
    color="white"
    ph={10}
    pv={20}
    style={{ borderWidth: .3, borderColor: '#d3d3d3' }}
    vcenter>
    <TView flex>
      <ThemedText size="md">{item.title}</ThemedText>
    </TView>
    <MaterialIcons name="arrow-forward-ios" size={20}/>
  </TView>
)

function Help({ navigation }) {
  const [state, actions] = useGlobal(['helpItems'])

  return (
    <TView color="white" flex>
      <FlatList
        data={state.helpItems}
        keyExtractor={(item) => `help-${item.id}`}
        renderItem={({ item }) =>
          <HelpListItem
            onPress={() => navigation.navigate("HelpDetails", { data: item })}
            item={item}
          />
        }
      />
    </TView>
  )
}

export default Help;
