/* @flow */

import React from 'react';
import { WebView } from 'react-native-webview';

function HelpDetails({ route }) {
  const { data } = route.params
  const htmlWrap = '<html><head><meta name="viewport" content="width=device-width, initial-scale=1"><style type="text/css">body {padding: 10px; font-family: "Helvetica Neue"} p {margin: 0px; padding: 0px}</style></head><body>';

  return (
    <WebView
      style={{ flex: 1 }}
      originWhitelist={['*']}
      source={{ html: htmlWrap + data.content + '</body></html>' }}
    />
  )
}

export default HelpDetails;
