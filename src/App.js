import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { MaterialIcons } from '@expo/vector-icons';

import HomeScreen from './screens/experiments/Home';
import WelcomeScreen from './screens/experiments/Welcome';
import SubscribeScreen from './screens/experiments/Subscribe';
import FoodScreen from './screens/experiments/Food';
import InfoScreen from './screens/experiments/Info';
import DetailScreen from './screens/experiments/Detail';
import ActivitiesScreen from './screens/experiments/Activities';
import FinishedFoodScreen from './screens/experiments/FinishedFood';
import FinishedScreen from './screens/experiments/Finished';
import CertificateScreen from './screens/experiments/Certificate';

import GalleryScreen from './screens/progress/Gallery';
import ProgressScreen from './screens/progress/Progress';

import HelpScreen from './screens/help/Help';
import HelpDetailsScreen from './screens/help/HelpDetails';

import MoreScreen from './screens/more/More';
import AccountScreen from './screens/more/Account';
import InvitationsScreen from './screens/more/Invitations';
import SettingsScreen from './screens/more/Settings';

const Stack = createNativeStackNavigator();
const options = {
    headerStyle: {backgroundColor: '#FFFF00',},
    headerTintColor: 'black',
    headerTitleAlign: 'center',
    headerBackTitle: "",
}

function ExperimentsStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen options={options} name="Welcome" component={WelcomeScreen}/>
      <Stack.Screen options={options} name="Home" component={HomeScreen} />
      <Stack.Screen options={options} name="Subscribe" component={SubscribeScreen}/>
      <Stack.Screen options={options} name="Food" component={FoodScreen}/>
      <Stack.Screen options={options} name="Info" component={InfoScreen}/>
      <Stack.Screen options={options} name="Detail" component={DetailScreen}/>
      <Stack.Screen options={options} name="Activities" component={ActivitiesScreen}/>
      <Stack.Screen options={options} name="FinishedFood" component={FinishedFoodScreen}/>
      <Stack.Screen options={options} name="Finished" component={FinishedScreen}/>
      <Stack.Screen options={options} name="Certificate" component={CertificateScreen}/>
    </Stack.Navigator>
  )
}

function ProgressStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen options={options} name="ProgressScreen" component={ProgressScreen}/>
      <Stack.Screen options={options} name="Gallery" component={GalleryScreen} />
    </Stack.Navigator>
  )
}

function HelpStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen options={options} name="HelpScreen" component={HelpScreen} />
      <Stack.Screen options={options} name="HelpDetails" component={HelpDetailsScreen}/>
    </Stack.Navigator>
  )
}

function MoreStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen options={options} name="More" component={MoreScreen} />
      <Stack.Screen options={options} name="Account" component={AccountScreen}/>
      <Stack.Screen options={options} name="Invitations" component={InvitationsScreen}/>
      <Stack.Screen options={options} name="Settings" component={SettingsScreen}/>
    </Stack.Navigator>
  )
}

const Tab = createBottomTabNavigator();

export default function App() {
  return (
      <Tab.Navigator
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;
            if (route.name === 'Experiments') {
              iconName = 'explore'
            } else if (route.name === 'Progress') {
              iconName = 'accessibility-new';
            } else if (route.name === 'Help') {
              iconName = 'contact-support';
            } else if (route.name === 'MoreScreen') {
              iconName = 'menu'
            }
            return <MaterialIcons name={iconName} size={25} color={color} />;
          },
          tabBarActiveTintColor: '#2196F3',
          tabBarInactiveTintColor: 'gray',
          headerShown: false,
          tabBarShowLabel: false
        })}>
        <Tab.Screen name="Experiments" component={ExperimentsStack}/>
        <Tab.Screen name="Progress" component={ProgressStack} />
        <Tab.Screen name="Help" component={HelpStack} />
        <Tab.Screen name="MoreScreen" component={MoreStack} />
      </Tab.Navigator>
  )
}
