import React from 'react';
import { TouchableOpacity, Text, TextInput } from 'react-native';
import { ThemedText, TView } from './Themed';
import { MaterialIcons } from '@expo/vector-icons'

import { createSpacers, createFlexers, createBorder } from './Styler';

export const Input = ({ ...rest }) => {
  return (
    <TView style={[{ padding: 10, borderWidth: 1, marginBottom: 20 }]}>
      <TextInput autoCapitalize="none" placeholderTextColor="#d3d3d3" {...rest} />
    </TView>
  )
}

export const CButton = ({ onPress, text, textColor, textSize, style, title, color, ...rest }) => (
  (
    text
    ? <TouchableOpacity style={[
        { alignItems: 'center' },
        createSpacers(rest),
        createFlexers(rest)]}
        onPress={onPress} {...rest}>
        <ThemedText bold size={textSize ? textSize : "sm"} color={textColor ? textColor : "#2196F3"}>{title}</ThemedText>
      </TouchableOpacity>
    : <TouchableOpacity
        style={[
          { backgroundColor: color ? color : "#2196F3", marginBottom: 10, height: 40, padding: 10, alignItems: 'center', justifyContent: 'center', borderRadius: 2 },
          createFlexers(rest),
          createSpacers(rest),
          style
        ]}
        onPress={onPress}
        {...rest}>
        <ThemedText color="white" size={textSize ? textSize : "sm"}>{title}</ThemedText>
      </TouchableOpacity>
  )
)

export const IconButton = ({ onPress, icon, color, size }) => (
  <TouchableOpacity onPress={onPress} style={{ margin: 5 }}>
    <MaterialIcons name={icon} size={size} color={color}/>
  </TouchableOpacity>
)
