import React from 'react';
import { View, KeyboardAvoidingView, ScrollView, Text, TouchableOpacity, ImageBackground } from 'react-native';

import { createSpacers, createFlexers, createBorder } from './Styler';

const variants = {
    card: { shadowRadius: 5, shadowOpacity: 0.3, shadowColor: '#d3d3d3', borderWidth: 1, borderColor: 'white', borderRadius: 8, shadowOffset: { height: 2, width: 1 } }
}

const fontSizes = {
    xs: 12,
    sm: 14,
    md: 16,
    lg: 20,
    h6: 24,
    h5: 28,
    h4: 30,
    h3: 32,
    h2: 34,
    h1: 40
}

export const ThemedText = ({ size = 'sm', bold, align, upper, italic, style, color, children, ...rest }) => (
    <Text {...rest} style={[
      {
        fontSize: fontSizes[size] ? fontSizes[size] : parseInt(size), color: color
      },
      (upper && { textTransform: 'uppercase' }),
      (bold && { fontWeight: 'bold' }),
      (align && { textAlign: align }),
      (italic && { fontStyle: 'italic' }),
      createSpacers(rest),
      createFlexers(rest),
      style]}>{children}</Text>
  )


export const TView = ({ children, pressable, onPress, imgUrl, bgcolor, withBackground, card, style, ...rest }) => (
  (
    pressable ?
      <TouchableOpacity {...rest} onPress={onPress} style={[
          { backgroundColor: bgcolor },
          ( card && variants.card ),
          createBorder(rest),
          createFlexers(rest),
          createSpacers(rest), style]}>
          {children}
      </TouchableOpacity>
    :
    (
      withBackground ?
        <ImageBackground
          source={require('../../assets/background.png')}
          style={[
            { flex: 1 },
            createFlexers(rest),
            createSpacers(rest),
             style]}>
          {children}
        </ImageBackground>
        :
        <View {...rest} style={[
          { backgroundColor: bgcolor },
          (card && variants.card),
          createBorder(rest),
          createFlexers(rest),
          createSpacers(rest), style]}>
          {children}
        </View>
    )
  )
)

export const KView = ({children, ...rest}) => {
    return (
        <KeyboardAvoidingView {...rest} style={{ flex: 1 }}>
            {children}
        </KeyboardAvoidingView>
    )
}

export const SView = ({children, style, ...rest}) => {
    return (
        <ScrollView {...rest}
          showsHorizontalScrollIndicator={false}
          style={[
            createFlexers(rest),
            createSpacers(rest),
            style]}>
            {children}
        </ScrollView>
    )
}
