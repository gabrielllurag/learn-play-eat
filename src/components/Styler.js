const spacers = {
  m: 'margin',
  mt: 'marginTop',
  ml: 'marginLeft',
  mr: 'marginRight',
  mb: 'marginBottom',
  mv: 'marginVertical',
  mh: 'marginHorizontal',
  p: 'padding',
  pt: 'paddingTop',
  pl: 'paddingLeft',
  pr: 'paddingRight',
  pb: 'paddingBottom',
  pv: 'paddingVertical',
  ph: 'paddingHorizontal'
}

export const createSpacers = props => Object.keys(spacers)
  .map(name => props[name] ? { [spacers[name]]: parseInt(props[name]) } : {})
  .reduce((o, i) => ({ ...o, ...i }), {})

const flexers = {
  flex: { flex: 1 },
  row: { flexDirection: 'row' },
  hcenter: { alignItems: 'center' },
  vcenter: { justifyContent: 'center' }
}

export const createFlexers = props => Object.keys(flexers)
  .map(name => props[name] ? flexers[name] : {})
  .reduce((o, i) => ({ ...o, ...i }), {})

const borders = {
  radius: 'borderRadius',
  border: 'borderWidth',
}

export const createBorder = props => Object.keys(borders)
  .map(name => props[name] ? { [borders[name]]: parseInt(props[name]) } : {})
  .reduce((o, i) => ({ ...o, ...i }), {})
